package id.potades.api.entity;

import lombok.Data;

@Data
public class Product {
    private String productSKU;
    private String productName;
    private Integer productStock;
}
