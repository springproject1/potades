package id.potades.api.publisher;

import id.potades.api.subscriber.Marketpalce;
import id.potades.api.subscriber.Shopee;
import id.potades.api.subscriber.Tokopedia;

import java.util.ArrayList;
import java.util.List;

public class NotifierSystem {
    private List<Marketpalce> marketpalces;

    public NotifierSystem() {
        if(marketpalces == null || marketpalces.size() == 0) {
            marketpalces = new ArrayList<>();
            marketpalces.add(new Shopee());
            marketpalces.add(new Tokopedia());
        }
    }

    public void notifyUpdateStock() {
        for(Marketpalce mp : marketpalces) {
            mp.updateStockSystem();
        }
    }
}
