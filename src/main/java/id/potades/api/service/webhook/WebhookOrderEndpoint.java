package id.potades.api.service.webhook;

import id.potades.api.publisher.NotifierSystem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/webhook/v1/new-order")
public class WebhookOrderEndpoint {

    @PostMapping("/tokped")
    private ResponseEntity tokpedOrderWebhook(@RequestBody HashMap<String, Object> body) {
        ResponseEntity response = ResponseEntity.accepted().build();

        Date start = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

        System.out.println("Start Webhook Service");
        System.out.println("Webhook triggered by Tokopedia on " + sdf.format(start) + " for new order");
        System.out.println("Request Body : " + body);

        System.out.println("Updating stock at local DB");
        // Here goes update stock at local DB
        System.out.println("Done update stock at local DB");

        System.out.println("Send update request to another marketplace");
        // Here goes pub-sub pattern to send API request
        NotifierSystem notifierSystem = new NotifierSystem();
        notifierSystem.notifyUpdateStock();
        System.out.println("Done update stock to another marketplace");

        printEndLogging(start);

        return response;
    }

    private void printEndLogging(Date start) {
        System.out.println("End webhook service, executed on " + (new Date().getTime() - start.getTime()) + "ms");
    }
}
